package com.company.BusinessLogic;

import com.company.DAO.OrderDAO;
import com.company.Model.Client;
import com.company.Model.Order;
import com.company.Model.Product;
import com.company.Bill.BillMaker;


import java.util.List;

/**
 * Created by Adi on 16-Apr-17.
 */
public class OrderProcessing {

    private static OrderDAO o = new OrderDAO();

    public static int placeOrder(Client client, Product product, int quantity){

        if (product.getStock() - quantity >= 0) {
            Order order = new Order(client.getId(), product.getId(), "Awaiting delivery", quantity);
            int orderid = OrderDAO.insertOrder(order);
            WarehouseAdministration.updateStockById(product.getId(), product.getStock() - quantity);
            order.setId(orderid);

            BillMaker bill = new BillMaker();
            bill.createBill(client, product, order);

            return 0;
        }else return -1;


    }

    public static List<Order> selectAllOrders(){
        return OrderDAO.selectAllOrders();
    }

    public static void removeOrderById(int id){
        OrderDAO.removeOrderById(id);
    }

    public static void updateStatus(String status, int id){
        OrderDAO.updateStatus(status, id);
    }

    public static Order findOrderById(int id){
        return o.findById(id);
    }



}
