package com.company.DAO;

import com.company.Connection.ConnectionFactory;
import com.company.Model.Client;

import java.sql.*;
import java.util.logging.*;
import java.util.*;

/**
 * Created by Adi on 15-Apr-17.
 */
public class ClientDAO extends AbstractDAO<Client>{


    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
    private final static String removeClientStatementString = "DELETE FROM client WHERE id=?";


    public static void removeClientById(int clientId){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;

        try{
            deleteStatement = dbConnection.prepareStatement(removeClientStatementString);
            deleteStatement.setInt(1, clientId);
            deleteStatement.executeUpdate();

        }catch (SQLException s){
            s.printStackTrace();
        }finally{
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }

    }

}
