package com.company.GUI;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by Adi on 23-Apr-17.
 */
public class AddClientView {

    private JFrame frame;
    private JButton finish = new JButton("Finish");
    private JButton cancel = new JButton("Cancel");
    JTextField nt;
    JTextField at;

    public AddClientView(){

        frame = new JFrame("Add new product");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setSize(720,360);


        JLabel name = new JLabel("Name: ");
        JLabel address = new JLabel("Address: ");

        nt = new JTextField(40);
        at = new JTextField(40);

        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();

        p1.add(name);
        p1.add(nt);
        p2.add(address);
        p2.add(at);


        p3.add(cancel);
        p3.add(finish);

        JPanel p =  new JPanel();

        p.add(p1);
        p.add(p2);
        p.add(p3);

        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));

        frame.setContentPane(p);
        frame.setVisible(true);

    }

    public String getName(){
        return nt.getText();
    }

    public String getAddress(){
        return at.getText();
    }

    public void dispose(){
        frame.dispose();
    }

    public void addCancelListener(ActionListener cl){
        cancel.addActionListener(cl);
    }

    public void addFinishListener(ActionListener fl){
        finish.addActionListener(fl);
    }

}
