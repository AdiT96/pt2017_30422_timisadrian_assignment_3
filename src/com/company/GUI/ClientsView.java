package com.company.GUI;

import com.company.BusinessLogic.ClientAdministration;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by Adi on 17-Apr-17.
 */
public class ClientsView {

    private JButton back = new JButton("Back");
    private JPanel p;
    private JFrame frame;
    private JTable table;
    private JScrollPane scroll;
    private JButton update = new JButton("Update Client");
    private JButton add = new JButton("Add Client");
    private JButton delete = new JButton("Delete Client");


    public ClientsView(){

        frame = new JFrame("Clients");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setSize(1080,720);

        table = Controller.createTable(ClientAdministration.selectAllClients());

        p = new JPanel();

        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();

        p1.add(back);

        scroll = new JScrollPane(table);
        p2.add(scroll);

        p3.add(add);
        p3.add(update);
        p3.add(delete);

        p.add(p1);
        p.add(p2);
        p.add(p3);

        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
        frame.setContentPane(p);
        frame.setVisible(false);

    }

    public int getSelectedRow(){
        return table.getSelectedRow();
    }

    public void addDeleteClientListener(ActionListener dl){
        delete.addActionListener(dl);
    }

    public void addInsertClientListener(ActionListener il){
        add.addActionListener(il);
    }

    public void addUpdateListener(ActionListener ul){
        update.addActionListener(ul);
    }

    public Object getValueFromTable(int row, int column){
        return table.getModel().getValueAt(row, column);
    }

    public void updateTable(){

        scroll.getViewport().remove(table);
        table = Controller.createTable(ClientAdministration.selectAllClients());
        scroll.getViewport().add(table);

    }

    public Object getSelectedClientId(){
        int row = table.getSelectedRow();
        if (row == -1) return null;
        return table.getModel().getValueAt(row, 0);
    }

    public void setVisibility(boolean b){
        frame.setVisible(b);
    }

    public void addBackListener(ActionListener bl){
        back.addActionListener(bl);
    }

}
