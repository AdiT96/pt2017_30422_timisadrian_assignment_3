package com.company.GUI;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by Adi on 23-Apr-17.
 */
public class ConfirmationUpdateProductsView {


    private JButton finish = new JButton("Finish");
    private JButton cancel = new JButton("Cancel");
    private JFrame frame;
    JTextField nt;
    JTextField st;
    JTextField dt;
    JTextField pt;

    public ConfirmationUpdateProductsView(){

        frame = new JFrame("Update Product");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setSize(720,360);


        JLabel name = new JLabel("Name: ");
        JLabel stock = new JLabel("Stock: ");
        JLabel description = new JLabel("Description: ");
        JLabel price = new JLabel("Price: ");

        nt = new JTextField(40);
        st = new JTextField(40);
        dt = new JTextField(40);
        pt = new JTextField(40);

        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();
        JPanel p4 = new JPanel();
        JPanel p5 = new JPanel();

        p1.add(name);
        p1.add(nt);
        p2.add(stock);
        p2.add(st);
        p3.add(description);
        p3.add(dt);
        p4.add(price);
        p4.add(pt);

        p5.add(cancel);
        p5.add(finish);

        JPanel p =  new JPanel();

        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);
        p.add(p5);

        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));

        frame.setContentPane(p);
        frame.setVisible(true);

    }

    public void addCancelListener(ActionListener cl){
        cancel.addActionListener(cl);
    }

    public void addOkListener(ActionListener ol){
        finish.addActionListener(ol);
    }

    public void dispose(){
        frame.dispose();
    }

    public void setName(Object n){
        nt.setText(n + "");
    }

    public void setStock(Object n){
        st.setText(n + "");
    }

    public void setDescription(Object n){
        dt.setText(n + "");
    }

    public void setPrice(Object n){
        pt.setText(n + "");
    }

    public String getName(){
        return nt.getText();
    }

    public int getStock(){
        int s;
        try{
            s = Integer.parseInt(st.getText());
        }catch(NumberFormatException n){
            return -1;
        }
        return s;
    }

    public String getDescription(){
        return dt.getText();
    }

    public int getPrice(){
        int p;
        try{
            p = Integer.parseInt(pt.getText());
        }catch(NumberFormatException n){
            return -1;
        }
        return p;
    }
}
