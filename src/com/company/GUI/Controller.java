package com.company.GUI;

import com.company.BusinessLogic.ClientAdministration;
import com.company.BusinessLogic.OrderProcessing;
import com.company.BusinessLogic.WarehouseAdministration;
import com.company.Model.Client;
import com.company.Model.Product;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Adi on 16-Apr-17.
 */
public class Controller {

    private View v;
    private WarehouseView wv;
    private ClientsView cv;
    private OrdersView ov;

    private SelectionErrorView sl;
    private UnderStockView uv;

    private AddProductView av;
    private DeleteProductView dpv;
    private ConfirmationUpdateProductsView cupv;

    private AddClientView acv;
    private DeleteClientView dcv;
    private UpdateClientView ucv;

    private PlaceOrderView pov;
    private CancelOrderView cov;
    private UpdateOrderStatusView uov;



    public Controller(View v, WarehouseView wv, ClientsView cv, OrdersView ov){
        this.v = v;
        this.wv = wv;
        this.cv = cv;
        this.ov = ov;

        v.addWarehouseListener(new WarehouseListener());
        v.addClientsListener(new ClientsListener());
        v.addOrdersListener(new OrdersListener());
        wv.addBackListener(new BackListenerWarehouse());
        wv.addInsertListener(new AddNewProduct());
        wv.addDeleteListener(new DeleteAProduct());
        wv.addUpdateListener(new UpdateProductsListener());
        cv.addBackListener(new BackListenerClients());
        cv.addInsertClientListener(new AddNewClient());
        cv.addUpdateListener(new UpdateClientListener());
        cv.addDeleteClientListener(new DeleteAClient());
        ov.addBackListener(new BackListenerOrders());
        ov.addPlaceOrderListener(new AddNewOrder());
        ov.addDeleteOrderListener(new CancelAnOrder());
        ov.addUpdateListener(new UpdateStatusListener());

    }

    public static JTable createTable(List<? extends Object> objects){

        JTable table = null;

        if (!objects.isEmpty()) {
            Class aClass = objects.get(0).getClass();

            Field[] fields = aClass.getDeclaredFields();


            String[] fieldsNames = new String[fields.length];
            for(int i = 0; i < fields.length; i++){
                fieldsNames[i] = fields[i].getName();
            }

            Object[][] data = new Object[objects.size()][fields.length];

            for(int i = 0; i < objects.size(); i++){

                for (int j = 0; j < fields.length; j++){

                    fields[j].setAccessible(true);
                    Object fieldsValue = null;
                    try{
                        fieldsValue = fields[j].get(objects.get(i));
                    }catch(IllegalAccessException e){
                        e.printStackTrace();
                    }

                    data[i][j] = fieldsValue;
                    fields[j].setAccessible(false);

                }

            }

            table = new JTable(data, fieldsNames);

        }

        return table;
    }

    class OkUpdateProductsListener implements ActionListener{
        public void actionPerformed(ActionEvent e){


            Product p = new Product(cupv.getName(), cupv.getStock(), cupv.getDescription(), cupv.getPrice());
            p.setId((Integer)wv.getSelectedProductId());
            WarehouseAdministration.updateProduct(p);

            wv.updateTable();

            cupv.dispose();

        }
    }

    class CancelUpdateProduct implements ActionListener{
        public void actionPerformed(ActionEvent e){
            cupv.dispose();
        }
    }

    class SelectionErrorListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            sl.dispose();
        }
    }

    class UpdateProductsListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            if (wv.getSelectedRow() >= 0) {
                cupv = new ConfirmationUpdateProductsView();
                cupv.setName(wv.getValueFromTable(wv.getSelectedRow(), 1));
                cupv.setStock(wv.getValueFromTable(wv.getSelectedRow(), 2));
                cupv.setDescription(wv.getValueFromTable(wv.getSelectedRow(), 3));
                cupv.setPrice(wv.getValueFromTable(wv.getSelectedRow(), 4));
                cupv.addOkListener(new OkUpdateProductsListener());
                cupv.addCancelListener(new CancelUpdateProduct());
            }else {
                sl = new SelectionErrorView();
                sl.addOkListener(new SelectionErrorListener());
            }
        }

    }

    class UpdateClientListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            if (cv.getSelectedRow() >= 0){

                ucv = new UpdateClientView();
                ucv.setName(cv.getValueFromTable(cv.getSelectedRow(), 1));
                ucv.setAddress(cv.getValueFromTable(cv.getSelectedRow(), 2));
                ucv.addCancelListener(new CancelUpdateClient());
                ucv.addOkListener(new OkUpdateClientListener());

            }else{
                sl = new SelectionErrorView();
                sl.addOkListener(new SelectionErrorListener());
            }
        }
    }

    class UpdateStatusListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            if (ov.getSelectedRow() >= 0){

                uov = new UpdateOrderStatusView();

                uov.setStatus(ov.getValueFromTable(ov.getSelectedRow(), 5));

                uov.addOkListener(new OkUpdateOrderStatusListener());
                uov.addCancelListener(new CancelUpdateOrder());

            }else{
                sl = new SelectionErrorView();
                sl.addOkListener(new SelectionErrorListener());
            }
        }
    }

    class OkUpdateOrderStatusListener implements ActionListener{
        public void actionPerformed(ActionEvent e){

            String status = uov.getStatus();
            int id = (Integer)ov.getSelectedOrderId();

            OrderProcessing.updateStatus(status, id);

            ov.updateTable();

            uov.dispose();

        }
    }

    class OkUpdateClientListener implements ActionListener{
        public void actionPerformed(ActionEvent e){

            Client c = new Client(ucv.getName(), ucv.getAddress());
            c.setId((Integer)cv.getSelectedClientId());
            ClientAdministration.updateClient(c);

            cv.updateTable();

            ucv.dispose();

        }
    }

    class CancelUpdateOrder implements ActionListener{
        public void actionPerformed(ActionEvent e){
            uov.dispose();
        }
    }

    class CancelUpdateClient implements ActionListener{
        public void actionPerformed(ActionEvent e){
            ucv.dispose();
        }
    }

    class FinishListenerInsertProduct implements ActionListener{
        public void actionPerformed(ActionEvent e){
            Product p = new Product(av.getName(), av.getStock(), av.getDescription(), av.getPrice());
            WarehouseAdministration.insertProduct(p);
            wv.updateTable();
            av.dispose();
        }
    }

    class CancelListenerInsertProduct implements ActionListener{
        public void actionPerformed(ActionEvent e){
            av.dispose();
        }
    }

    class CancelDeleteProduct implements ActionListener{
        public void actionPerformed(ActionEvent e){
            dpv.dispose();
        }
    }

    class OkProductDeleteListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            int id = (Integer)wv.getSelectedProductId();
            WarehouseAdministration.removeProductById(id);
            wv.updateTable();
            dpv.dispose();
        }
    }

    class DeleteAProduct implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            if (wv.getSelectedRow() >= 0) {
                dpv = new DeleteProductView();
                dpv.addCancelListener(new CancelDeleteProduct());
                dpv.addOkListener(new OkProductDeleteListener());
            }else {
                sl = new SelectionErrorView();
                sl.addOkListener(new SelectionErrorListener());
            }
        }
    }

    class OkClientDeleteListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            int id = (Integer)cv.getSelectedClientId();
            ClientAdministration.removeClient(id);
            cv.updateTable();
            dcv.dispose();
        }
    }

    class CancelDeleteClientListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            dcv.dispose();
        }
    }

    class DeleteAClient implements ActionListener{
        public void actionPerformed(ActionEvent e){
            if(cv.getSelectedRow() >= 0){

                dcv = new DeleteClientView();
                dcv.addCancelListener(new CancelDeleteClientListener());
                dcv.addOkListener(new OkClientDeleteListener());

            }else {
                sl = new SelectionErrorView();
                sl.addOkListener(new SelectionErrorListener());
            }
        }
    }

    class CancelAnOrder implements ActionListener{
        public void actionPerformed(ActionEvent e){
            if(ov.getSelectedRow() >= 0){

                cov = new CancelOrderView();
                cov.addOkListener(new OkCancelOrder());
                cov.addCancelListener(new CancelCancelOrder());

            }else {
                sl = new SelectionErrorView();
                sl.addOkListener(new SelectionErrorListener());
            }
        }
    }

    class OkCancelOrder implements ActionListener{
        public void actionPerformed(ActionEvent e){

            int id = (Integer)ov.getSelectedOrderId();
            OrderProcessing.removeOrderById(id);

            ov.updateTable();

            cov.dispose();

        }
    }

    class CancelCancelOrder implements ActionListener{
        public void actionPerformed(ActionEvent e){
            cov.dispose();
        }
    }

    class AddNewProduct implements ActionListener{
        public void actionPerformed(ActionEvent e){
            av = new AddProductView();
            av.addCancelListener(new CancelListenerInsertProduct());
            av.addFinishListener(new FinishListenerInsertProduct());
        }
    }

    class AddNewOrder implements ActionListener{
        public void actionPerformed(ActionEvent e){
            pov = new PlaceOrderView();
            pov.addCancelListener(new CancelAddOrder());
            pov.addFinishListener(new FinishPlaceOrder());
        }
    }

    class FinishPlaceOrder implements ActionListener{
        public void actionPerformed(ActionEvent e){
            if(pov.getSelectedRowClient() >=0 && pov.getSelectedRowProduct() >= 0){

                int row = pov.getSelectedRowClient();

                String name = pov.getClientName(row);
                String address = pov.getClientAddress(row);
                Client c = new Client(name, address);
                c.setId(pov.getClientId(row));

                row = pov.getSelectedRowProduct();

                String pname = pov.getProductName(row);
                int stock = pov.getProductStock(row);
                String description = pov.getProductDescription(row);
                int price = pov.getProductPrice(row);
                Product p = new Product(pname, stock, description, price);
                p.setId(pov.getProductId(row));

                if (OrderProcessing.placeOrder(c, p, pov.getQuantity()) == 0) {

                    pov.updateTables();

                    ov.updateTable();

                    wv.updateTable();
                }else {
                    uv = new UnderStockView();
                    uv.addOkListener(new OkUnderStockListener());
                }

                pov.dispose();


            }else{
                sl = new SelectionErrorView();
                sl.addOkListener(new SelectionErrorListener());
            }
        }
    }

    class OkUnderStockListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            uv.dispose();
        }
    }

    class CancelAddOrder implements ActionListener{
        public void actionPerformed(ActionEvent e){
            pov.dispose();
        }
    }

    class AddNewClient implements ActionListener{
        public void actionPerformed(ActionEvent e){
            acv = new AddClientView();
            acv.addCancelListener(new CancelClient());
            acv.addFinishListener(new FinishAddClientListener());
        }
    }

    class FinishAddClientListener implements ActionListener{
        public void actionPerformed(ActionEvent e){

            Client c = new Client(acv.getName(), acv.getAddress());
            ClientAdministration.addClient(c);
            cv.updateTable();
            acv.dispose();
        }
    }

    class CancelClient implements ActionListener{
        public void actionPerformed(ActionEvent e){
            acv.dispose();
        }
    }

    class BackListenerOrders implements ActionListener{
        public void actionPerformed(ActionEvent e){
            ov.setVisibility(false);
        }
    }

    class BackListenerClients implements ActionListener{
        public void actionPerformed(ActionEvent e){
            cv.setVisibility(false);
        }
    }

    class BackListenerWarehouse implements ActionListener{
        public void actionPerformed(ActionEvent e){
            wv.dispose();
        }
    }

    class OrdersListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            ov.setVisibility(true);
        }
    }

    class ClientsListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            cv.setVisibility(true);
        }
    }

    class WarehouseListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            wv.setVisibility(true);
        }
    }

}
