package com.company.GUI;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by Adi on 23-Apr-17.
 */
public class DeleteProductView {

    JButton ok = new JButton("OK");
    JButton cancel = new JButton("Cancel");
    JFrame frame;

    public DeleteProductView(){

        frame = new JFrame("Remove Product");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setSize(720,360);

        JLabel label = new JLabel("Are you sure you would like to remove that product?");

        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();

        p1.add(label);
        p2.add(ok);
        p2.add(cancel);

        JPanel p = new JPanel();

        p.add(p1);
        p.add(p2);

        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));

        frame.setContentPane(p);
        frame.setVisible(true);

    }

    public void addCancelListener(ActionListener cl){
        cancel.addActionListener(cl);
    }

    public void addOkListener(ActionListener ol){
        ok.addActionListener(ol);
    }

    public void dispose(){
        frame.dispose();
    }

}
