package com.company.GUI;

import com.company.BusinessLogic.OrderProcessing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by Adi on 17-Apr-17.
 */
public class OrdersView {

    private JButton back = new JButton("Back");
    private JPanel p;
    private JFrame frame;
    private JTable table;
    private JScrollPane scroll;
    private JButton placeOrder = new JButton("Place Order");
    private JButton cancel = new JButton("Cancel Order");
    private JButton status = new JButton("Change Status");

    public OrdersView(){

        frame = new JFrame("Orders");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setSize(1080,720);

        table = Controller.createTable(OrderProcessing.selectAllOrders());
        table.setSize(new Dimension(100, 0));

        scroll = new JScrollPane(table,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        //JScrollBar bar = scroll.getVerticalScrollBar();
        //bar.setPreferredSize(new Dimension(200, 0));

        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();

        p1.add(back);

        p2.add(scroll);

        p3.add(placeOrder);
        p3.add(cancel);
        p3.add(status);

        p = new JPanel();
        p.add(p1);
        p.add(p2);
        p.add(p3);

        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
        frame.setContentPane(p);
        frame.setVisible(false);

    }

    public void setVisibility(boolean b){
        frame.setVisible(b);
    }

    public void addBackListener(ActionListener bl){
        back.addActionListener(bl);
    }

    public int getSelectedRow(){
        return table.getSelectedRow();
    }

    public void addDeleteOrderListener(ActionListener dl){
        cancel.addActionListener(dl);
    }

    public void addPlaceOrderListener(ActionListener il){
        placeOrder.addActionListener(il);
    }

    public void addUpdateListener(ActionListener ul){
        status.addActionListener(ul);
    }

    public Object getValueFromTable(int row, int column){
        return table.getModel().getValueAt(row, column);
    }

    public void updateTable(){

        scroll.getViewport().remove(table);
        table = Controller.createTable(OrderProcessing.selectAllOrders());
        scroll.getViewport().add(table);

    }

    public Object getSelectedOrderId(){
        int row = table.getSelectedRow();
        if (row == -1) return null;
        return table.getModel().getValueAt(row, 0);
    }

}
