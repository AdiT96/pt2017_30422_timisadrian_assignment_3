package com.company.GUI;

import com.company.BusinessLogic.ClientAdministration;
import com.company.BusinessLogic.OrderProcessing;
import com.company.BusinessLogic.WarehouseAdministration;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by Adi on 23-Apr-17.
 */
public class PlaceOrderView {

    private JFrame frame;
    private JButton finish = new JButton("Finish");
    private JButton cancel = new JButton("Cancel");
    private JTextField qt;
    private JTable table1;
    private JTable table2;
    JScrollPane scroll1;
    JScrollPane scroll2;

    public PlaceOrderView(){

        frame = new JFrame("Place a new Order");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setSize(1080,720);

        table1 = Controller.createTable(ClientAdministration.selectAllClients());
        scroll1 = new JScrollPane(table1);

        table2 = Controller.createTable(WarehouseAdministration.selectAllProducts());
        scroll2 = new JScrollPane(table2);

        JPanel p1 = new JPanel();
        p1.add(scroll1);
        p1.add(scroll2);

        JPanel p2 = new JPanel();
        p2.add(finish);
        p2.add(cancel);

        JPanel p3 = new JPanel();
        JLabel quantity = new JLabel("Quantity");
        qt = new JTextField(5);
        qt.setText("1");
        p3.add(quantity);
        p3.add(qt);

        JPanel p =  new JPanel();
        p.add(p1);
        p.add(p3);
        p.add(p2);

        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));

        frame.setContentPane(p);
        frame.setVisible(true);

    }

    public int getSelectedRowClient(){
        return table1.getSelectedRow();
    }

    public int getSelectedRowProduct(){
        return table2.getSelectedRow();
    }

    public String getClientName(int row){
        return (String)table1.getModel().getValueAt(row, 1);
    }

    public String getClientAddress(int row){
        return (String)table1.getModel().getValueAt(row, 2);
    }

    public int getClientId(int row){
        return (Integer)table1.getModel().getValueAt(row, 0);
    }

    public String getProductName(int row){
        return (String)table2.getModel().getValueAt(row, 1);
    }

    public int getProductStock(int row){
        return (Integer)table2.getModel().getValueAt(row, 2);
    }

    public String getProductDescription(int row){
        return (String)table2.getModel().getValueAt(row, 3);
    }

    public int getProductPrice(int row){
        return (Integer)table2.getModel().getValueAt(row, 4);
    }

    public int getQuantity(){
        return Integer.parseInt(qt.getText());
    }

    public int getProductId(int row){
        return (Integer)table2.getModel().getValueAt(row, 0);
    }

    public void updateTables(){

        scroll1.getViewport().remove(table1);
        table1 = Controller.createTable(ClientAdministration.selectAllClients());
        scroll1.getViewport().add(table1);

        scroll2.getViewport().remove(table2);
        table2 = Controller.createTable(WarehouseAdministration.selectAllProducts());
        scroll2.getViewport().add(table2);

    }

    public void dispose(){
        frame.dispose();
    }

    public void addCancelListener(ActionListener cl){
        cancel.addActionListener(cl);
    }

    public void addFinishListener(ActionListener fl){
        finish.addActionListener(fl);
    }

}
