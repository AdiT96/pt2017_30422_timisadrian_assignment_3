package com.company.GUI;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by Adi on 23-Apr-17.
 */
public class SelectionErrorView {

    private JButton ok = new JButton("Ok");
    private JLabel label = new JLabel("Selection error: please select the object you would like to interact with");
    private JFrame frame;

    public SelectionErrorView(){

        frame = new JFrame("Selection Error");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setSize(720,360);

        JPanel p1 = new JPanel();
        p1.add(label);
        JPanel p2 = new JPanel();
        p2.add(ok);

        JPanel p = new JPanel();

        p.add(p1);
        p.add(p2);

        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));

        frame.setContentPane(p);
        frame.setVisible(true);

    }

    public void addOkListener(ActionListener ol){
        ok.addActionListener(ol);
    }

    public void dispose(){
        frame.dispose();
    }


}
