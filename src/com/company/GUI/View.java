package com.company.GUI;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by Adi on 16-Apr-17.
 */
public class View {

    private JButton warehouse = new JButton("Warehouse Management");
    private JButton clients = new JButton("Client Management");
    private JButton order = new JButton("Order Management");
    private JPanel p;
    private JFrame frame;


    public View(){

        frame = new JFrame("Store Management");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1080, 720);

        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();

        p1.add(warehouse);
        p2.add(clients);
        p3.add(order);

        p = new JPanel();
        p.add(p1);
        p.add(p2);
        p.add(p3);


        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));

        frame.setContentPane(p);
        frame.setVisible(true);

    }

    public void addOrdersListener(ActionListener ol){
        order.addActionListener(ol);
    }

    public void addClientsListener(ActionListener cl){
        clients.addActionListener(cl);
    }

    public void addWarehouseListener(ActionListener wl){
        warehouse.addActionListener(wl);
    }

}
