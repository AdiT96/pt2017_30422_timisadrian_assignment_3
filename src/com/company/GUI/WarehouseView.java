package com.company.GUI;

import com.company.BusinessLogic.WarehouseAdministration;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionListener;

/**
 * Created by Adi on 16-Apr-17.
 */
public class WarehouseView {

    private JButton back = new JButton("Back");
    private JPanel p;
    private JFrame frame;
    private JTable table;
    private JScrollPane scroll;
    private JButton add = new JButton("Add New Product");
    private JButton update = new JButton("Update Product");
    private JButton delete = new JButton("Delete Product");



    public WarehouseView(){
        frame = new JFrame("Warehouse");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setSize(1080,720);

        table = Controller.createTable(WarehouseAdministration.selectAllProducts());

        p = new JPanel();

        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();

        p1.add(back);
        scroll = new JScrollPane(table);
        p2.add(scroll);
        p3.add(add);
        p3.add(update);
        p3.add(delete);

        p.add(p1);
        p.add(p2);
        p.add(p3);

        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
        frame.setContentPane(p);
        frame.setVisible(false);

    }

    public int getSelectedRow(){
        return table.getSelectedRow();
    }

    public void addUpdateListener(ActionListener ul){
        update.addActionListener(ul);
    }

    public Object getValueFromTable(int row, int column){
        return table.getModel().getValueAt(row, column);
    }

    public int getTableRows(){
        return table.getModel().getRowCount();
    }

    public void updateTable(){

        scroll.getViewport().remove(table);
        table = Controller.createTable(WarehouseAdministration.selectAllProducts());
        scroll.getViewport().add(table);

    }

    public Object getSelectedProductId(){
        int row = table.getSelectedRow();
        if (row == -1) return null;
        return table.getModel().getValueAt(row, 0);
    }

    public void addDeleteListener(ActionListener dl){
        delete.addActionListener(dl);
    }

    public void addInsertListener(ActionListener al){
        add.addActionListener(al);
    }

    public void setVisibility(boolean b){

        frame.setVisible(b);
    }

    public void dispose(){
        frame.dispose();
    }

    public void addBackListener(ActionListener bl){
        back.addActionListener(bl);
    }

    public JTable getTable() {
        return table;
    }
}
