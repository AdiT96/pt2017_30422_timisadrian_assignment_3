package com.company;

import com.company.GUI.*;

public class Main {

    public static void main(String[] args) {

        View v = new View();
        WarehouseView wv = new WarehouseView();
        ClientsView cv = new ClientsView();
        OrdersView ov = new OrdersView();
        Controller c = new Controller(v, wv, cv, ov);

    }
}
