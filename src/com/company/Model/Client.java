package com.company.Model;

/**
 * Created by Adi on 15-Apr-17.
 */
public class Client {

    private int id;
    private String name;
    private String address;

    public Client(){

    }

    public Client(int i, String n, String a){

        super();

        id = i;
        name = n;
        address = a;

    }

    public Client(String n, String a){

        super();

        name = n;
        address = a;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String toString(){

        return "Client [id=" + id + ", name=" + name + ", address=" + address + "]";


    }
}
